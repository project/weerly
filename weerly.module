<?php

/**
 * @file
 * weerly.module
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\weerly\WeerlyHelper;

/**
 * Implements hook_ENTITY_TYPE_create().
 */
function weerly_node_create(EntityInterface $entity) {
  if ($entity->bundle() == 'weerly') {
    // Set a temporary weerly title.
    $entity->setTitle('Weerly-New');
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function weerly_node_presave(EntityInterface $entity) {
  if ($entity->bundle() == 'weerly' && !$entity->isNew()) {
    _weerly_url_update($entity);
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function weerly_node_insert(EntityInterface $entity) {
  if ($entity->bundle() == 'weerly') {
    _weerly_url_update($entity);
  }
}

/**
 * Implements weerly URL update function.
 */
function _weerly_url_update(EntityInterface $entity) {
  if ($entity->bundle() == 'weerly') {
    if (($entity instanceof ContentEntityInterface) && $entity->hasField('path')) {
      if ($entity instanceof RevisionableInterface && $entity->isDefaultRevision()) {
        $source_path = '/node/' . $entity->id();
        $langcode = 'und';
        $alias = WeerlyHelper::parseEncode($entity->id());
        $old_alias = NULL;

        $weerly_source = $entity->get('field_weerly_source')->get(0);
        if ($weerly_source !== NULL) {
          $weerly_source = $weerly_source->getValue();
          $alias = $weerly_source['value'];
        }
        $default_alias = [
          'path' => $source_path,
          'alias' => '/' . $alias,
          'langcode' => $langcode,
          'pathauto' => 0,
        ];

        // After update.
        if ($entity->original !== NULL) {
          $old_weerly_source = $entity->original->get('field_weerly_source')->get(0);
          if ($old_weerly_source !== NULL) {
            $old_weerly_source = $old_weerly_source->getValue();
            $old_alias = $old_weerly_source['value'];
          }
          if ($alias != $old_alias || $old_alias == NULL || empty($alias)) {
            $entity->setTitle('Weerly: /' . $alias);
            $entity->set('path', $default_alias);
            $entity->set('field_weerly_source', $alias);
            $old_path_alias = [
              'path' => $source_path,
              'alias' => '/' . $old_alias,
            ];
            drupal_register_shutdown_function('_weerly_remove_path_alias', $old_path_alias);
          }
        }
        else {
          // After insert.
          $entity->setTitle('Weerly: /' . $alias);
          $entity->set('path', $default_alias);
          $entity->set('field_weerly_source', $alias);
          $entity->save();
        }
      }
    }
  }
}

/**
 * Implements hook_entity_bundle_field_info_alter().
 */
function weerly_entity_bundle_field_info_alter(&$fields, EntityTypeInterface $entity_type, $bundle) {
  if ($entity_type->id() == 'node' && $bundle === 'weerly' && isset($fields['field_weerly_source'])) {
    $fields['field_weerly_source']->addConstraint('WeerlyUrlChecking', []);
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function weerly_form_node_weerly_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  _weerly_form_alter($form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function weerly_form_node_weerly_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  _weerly_form_alter($form);
}

/**
 * Implements some form fields modifications.
 */
function _weerly_form_alter(&$form) {
  $host = \Drupal::request()->getSchemeAndHttpHost();
  $form['field_weerly_source']['widget']['0']['#type'] = 'details';
  $form['field_weerly_source']['widget']['0']['value']['#title'] = $host . '/';
  unset($form['field_weerly_source']['widget']['0']['value']['#description']);
}

/**
 * Implements hook_theme().
 */
function weerly_theme($existing, $type, $theme, $path) {
  return [
    'field__field_weerly_source' => [
      'template' => 'field--field-weerly-source',
      'base hook' => 'node',
    ],
  ];
}

/**
 * Implements hook_preprocess().
 */
function weerly_preprocess(&$variables) {
  $variables['base_path'] = \Drupal::request()->getSchemeAndHttpHost();
}

/**
 * Implements a path alias remover.
 */
function _weerly_remove_path_alias(array $path_alias) {
  $path_alias_storage = \Drupal::entityTypeManager()->getStorage('path_alias');

  // Load all path alias by path and alias.
  $alias_objects = $path_alias_storage->loadByProperties([
    'path' => $path_alias['path'],
    'alias' => $path_alias['alias'],
  ]);

  // Delete a path alias.
  foreach ($alias_objects as $path_alias_object) {
    $path_alias_object->delete();
  }

}
