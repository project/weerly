<?php

namespace Drupal\weerly\EventSubscriber;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * Redirect whenever a 403 meets the criteria for unpublished nodes.
 */
class WeerlyRedirectOn403Subscriber extends HttpExceptionSubscriberBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManger;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Constructs a new WeerlyRedirectOn403Subscriber.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match.
   */
  public function __construct(AccountInterface $current_user, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, CurrentRouteMatch $current_route_match) {
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
    $this->entityTypeManger = $entity_type_manager;
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats() {
    return ['html'];
  }

  /**
   * Redirects on 403 Access Denied kernel exceptions.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event
   *   The Event to process.
   */
  public function on403(GetResponseForExceptionEvent $event) {

    $request = clone $event->getRequest();
    if ($request->attributes->get('node') != NULL) {
      $nid = $this->currentRouteMatch->getRawParameter('node');
      $node = $this->entityTypeManger->getStorage('node')->load($nid);
      $node_type = $node->getType();
      $is_published = $node->isPublished();
      $is_anonymous = $this->currentUser->isAnonymous();

      if ($node_type == 'weerly' && !$is_published && $is_anonymous) {
        // Determine the url options.
        $options = [
          'absolute' => TRUE,
        ];
        $redirect_path = Url::fromRoute('<front>', [], $options)->toString();
        $response_code = '307';
        $cacheMetadata = CacheableMetadata::createFromObject($node)
          ->addCacheTags(['rendered']);
        $response = new TrustedRedirectResponse($redirect_path, $response_code);
        $response->addCacheableDependency($cacheMetadata);
        // Set response as not cacheable, otherwise browser will cache it.
        $response->setCache(['max_age' => 0]);

        $event->setResponse($response);

      }

    }

  }

}
