<?php

namespace Drupal\weerly\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Drupal\Core\Path\CurrentPathStack;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event handler to process redirection of a weerly URL content.
 */
class WeerlyRedirect implements EventSubscriberInterface {

  /**
   * Currently signed in user.
   *
   * @var Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManger;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Constructs a WeerlyRedirect object.
   *
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   */
  public function __construct(AccountProxy $current_user, EntityTypeManagerInterface $entity_type_manager, CurrentPathStack $current_path) {
    $this->currentUser = $current_user;
    $this->entityTypeManger = $entity_type_manager;
    $this->currentPath = $current_path;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // This announces which events you want to subscribe to.
    return([
      KernelEvents::REQUEST => [
        ['redirect', 210],
      ],
    ]);
  }

  /**
   * Redirect requests for `weerly URL` nodes to their destination url.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Event object of current event.
   */
  public function redirect(GetResponseEvent $event) {
    $path = $this->currentPath->getPath();
    $current_path = str_replace("/", "", $path);

    $path_alias_storage = $this->entityTypeManger->getStorage('path_alias');

    // Load all path alias for this entity.
    $alias_objects = $path_alias_storage->loadByProperties([
      'alias' => '/' . $current_path,
    ]);

    foreach ($alias_objects as $alias_object) {
      $nid = str_replace('/node/', '', $alias_object->get('path')->value);
      break;
    }

    if (!empty($nid)) {
      $node = $this->entityTypeManger->getStorage('node')->load($nid);
    }

    // Only redirect weerly URL content.
    if (empty($node) || $node->getType() !== 'weerly') {
      return;
    }

    $roles = $this->entityTypeManger->getStorage('user_role')->loadMultiple($this->currentUser->getRoles());
    $isAdmin = array_reduce($roles, function ($carry, $item) {
      return $carry || $item->isAdmin();
    });

    // If user can edit the page, or is an admin, they should not be redirected.
    // If a user can't view the node, they should not be redirect and get a 403.
    if ($isAdmin || !$node->access() || $node->access('edit')) {
      return;
    }

    // Get the destination url.
    $destination = $node->get('field_weerly_destination')->first();
    if ($destination != NULL) {
      $destination = $destination->getUrl()->toString();
    }

    if (!empty($destination)) {

      // Convert e.g. internal:/ scheme paths.
      $destination = Url::fromUri($destination)->toString();

      // Create Redirect to the external URL.
      $response = new TrustedRedirectResponse($destination);

      // Don't Cache the redirect.
      $build = [
        '#cache' => [
          'contexts' => ['user']
        ],
      ];
      $response->addCacheableDependency($build);

      $node->field_weerly_hit_counter->value += 1;
      $node->field_weerly_last_used->value = time();
      $node->save();

      // Execute the redirect.
      $event->setResponse($response);

    }
  }

}
