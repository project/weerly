<?php

namespace Drupal\weerly\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Weerly URL constraint.
 *
 * Validation component for weerly urls suffixes.
 *
 * @Constraint(
 *   id = "WeerlyUrlChecking",
 *   label = @Translation("Weerly URL constraint", context = "Validation"),
 * )
 */
class WeerlyUrlCheckingConstraint extends Constraint {

  /**
   * {@inheritdoc}
   */
  public $notValidFormat = '%value is not valid. Only letters, numbers, underscore and dash are allowed.';

  /**
   * {@inheritdoc}
   */
  public $notUnique = '%value is not unique. %info.';

}
