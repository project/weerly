<?php

namespace Drupal\weerly;

/**
 * Helper convert a number short alpha-numeric value.
 *
 * @see
 *   https://seanreiser.com/blog/note/algorithmic-url-shortening-drupal-7-8-and-9
 */
class WeerlyHelper {

  /**
   * Encodes a number to alpha-numeric value.
   *
   * @param int $num
   *   The number to convert.
   *
   * @return string
   *   The alpha-numeric value.
   */
  public static function parseEncode(int $num) {
    $index = "23456789bcdfghjkmnpqrstvwxzBCDFGHJKLMNPQRSTVWXZ";
    $base = strlen($index);
    // Force a large number to get least three chars length.
    $num *= 10000;
    $out = "";
    for ($t = floor(log10($num) / log10($base)); $t >= 0; $t--) {
      $a = floor($num / pow($base, $t));
      $out = $out . substr($index, $a, 1);
      $num = $num - ($a * pow($base, $t));
    }

    return $out;
  }

  /**
   * Decodes the alpha-numeric value to th number.
   *
   * @param string $value
   *   The alpha-numeric value.
   *
   * @return int
   *   The decoded number value.
   */
  public static function parseDecode(string $value) {
    // @todo check if the value is an valid enconded string.
    $num = $value;
    $index = "23456789bcdfghjkmnpqrstvwxzBCDFGHJKLMNPQRSTVWXZ";
    $base = strlen($index);
    $out = 0;
    $len = strlen($num) - 1;
    for ($t = 0; $t <= $len; $t++) {
      $out = $out + strpos($index, substr($num, $t, 1)) * pow($base, $len - $t);
    }

    return $out / 10000;
  }

}
