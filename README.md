# Weerly

Weerly is a simple URL shortening alternative. For those waiting a while
to a migration path from shurly maybe the weerly is for you. It is more
simple than shurly because do not concern about flood crontrol or block
shortening the phishing URLs, for example. However, weerly module was
made over the native Drupal Entity API then you can integrate any securtiy
or control engine avalaible for that.

## Usage

- Download and install the module as usual. 
- If required export from the old Drupal 7 database your shurly URLs to a CSV
  file (SQL example):
```
    SELECT
        source as field_weerly_source
        destination as field_weerly_destination,
        count as field_weerly_hit_counter,
        last_used as field_weerly_last_used
    FROM shurly
```

- Import the CVS file using your preferable method (weerly URL content), 
  I recommend use the feeds module.

- List your weerly URLs at /weerly-urls
- Add new URL at /node/add/weerly

## Additional information

If you need support file a issue.
